import React from 'react';
import { Layout, Menu, Icon } from 'antd';

const { Header } = Layout;

class HeaderPart extends React.Component {

    state = {
        current: 'profile'
    };

    handleClick = e => {
        console.log('click ', e);
        this.setState({
            current: e.key
        });
    };

    render() {
        return (
            <Header style={{ padding: '0px 100px' }} id="background">
                <Menu onClick={this.handleClick} mode="horizontal" id="background">
                    <Menu.Item style={{ marginTop: "1%" }} disabled>
                        <img src={"/5002Assets/camt_horizontal.png"} id="logo" />
                    </Menu.Item>
                    <Menu.Item key="profile">
                        <a href="/profile" id="menu_color">
                            <Icon type="user" />
                            Profile
                            </a>
                    </Menu.Item>
                    <Menu.Item key="anime">
                        <a href="/anime" id="menu_color">
                            <Icon type="inbox" />
                            Anime
                            </a>
                    </Menu.Item>
                    <Menu.Item key="report">
                        <a href="/report" id="menu_color">
                            <Icon type="container" />
                            Study Report
                            </a>
                    </Menu.Item>
                </Menu>
            </Header>
        )
    }
}

export default HeaderPart