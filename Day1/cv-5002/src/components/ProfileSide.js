import React from 'react';
import { FaFacebookF, FaTwitter, FaInstagram } from "react-icons/fa";
import { Typography, Layout } from 'antd';

const { Title, Text } = Typography;
const { Sider } = Layout;

class ProfileSide extends React.Component {
    render() {
        return (
            <Sider id="background">
                <div>
                    <img src="/5002Assets/Nanprofile.jpg" id="img-profile" />
                </div>
                <div>
                    <Title level={3} style={{ color: "#48DA9B" }}>Contact details</Title>
                    <br />
                    <Text strong style={{ color: "#48DA9B" }}>Phone:</Text>
                    <br />
                    <Text style={{ color: "white" }}>091-7919608</Text>
                    <br />
                    <Text strong style={{ color: "#48DA9B" }}>Email:</Text>
                    <br />
                    <Text style={{ color: "white" }}>akoranos@gmail.com</Text>
                    <br />
                    <Text strong style={{ color: "#48DA9B" }}>Adress:</Text>
                    <br />
                    <Text style={{ color: "white" }}>San Kamphaeng, Chiang Mai, Thailand</Text>
                </div>
                <div className="wrapper">
                    <br />
                    <Title level={3} style={{ color: "#48DA9B" }}>Socials</Title>
                    <ul>
                        <a href="https://www.facebook.com/Akoros.Kull"><li className="facebook"><i className="fa fa-facebook fa-2x" aria-hidden="true"><FaFacebookF /></i></li></a>
                        <a href="https://twitter.com/akoranos"><li className="twitter"><i className="fa fa-twitter fa-2x" aria-hidden="true"><FaTwitter /></i></li></a>
                        <a href="https://www.instagram.com/akoranos/"><li className="instagram"><i className="fa fa-instagram fa-2x" aria-hidden="true"><FaInstagram /></i></li></a>
                    </ul>
                </div>
            </Sider>
        )
    }
}

export default ProfileSide