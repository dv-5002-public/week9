import React from 'react';
import { Table, BackTop, Input, Select, Card, Col, Row, Typography, Pagination, Spin, Icon, Tooltip } from 'antd';

const { Title } = Typography;
const { Option } = Select;

const ReportTable = (props) => {
    const TableData = [
        {
            semester: "1",
            year: "2560",
            enrolled: [
                { courseNo: "001101", courseTitle: "FUNDAMENTAL ENGLISH 1", credit: 3.00, grade: "B" },
                { courseNo: "206113", courseTitle: "CAL FOR SOFTWARE ENGINEERING", credit: 3.00, grade: "W" },
                { courseNo: "751100", courseTitle: "ECONOMICS FOR EVERYDAY LIFE", credit: 3.00, grade: "C" },
                { courseNo: "951100", courseTitle: "MODERN LIFE AND ANIMATION", credit: 3.00, grade: "B+" },
                { courseNo: "953103", courseTitle: "PROGRAMMING LOGICAL THINKING", credit: 2.00, grade: "C+" },
                { courseNo: "953211", courseTitle: "COMPUTER ORGANIZATION", credit: 3.00, grade: "B" }
            ],
            gpa: [
                { record: "Semester 1/2560", ca: 17.00, ce: 14.00, gpa: 2.82, totalGpa: 2.82 }
            ]
        },
        {
            semester: "2",
            year: "2560",
            enrolled: [
                { courseNo: "001102", courseTitle: "FUNDAMENTAL ENGLISH 2", credit: 3.00, grade: "B+" },
                { courseNo: "011251", courseTitle: "LOGIC", credit: 3.00, grade: "B+" },
                { courseNo: "206113", courseTitle: "CAL FOR SOFTWARE ENGINEERING", credit: 3.00, grade: "C" },
                { courseNo: "953102", courseTitle: "ADT & PROBLEM SOLVING", credit: 3.00, grade: "B" },
                { courseNo: "953104", courseTitle: "WEB UI DESIGN & DEVELOP", credit: 2.00, grade: "C+" },
                { courseNo: "953202", courseTitle: "INTRODUCTION TO SE", credit: 3.00, grade: "C" },
                { courseNo: "953231", courseTitle: "OBJECT ORIENTED PROGRAMMING", credit: 3.00, grade: "C+" },
                { courseNo: "955100", courseTitle: "LEARNING THROUGH ACTIVITIES 1", credit: 1.00, grade: "A" }
            ],
            gpa: [
                { record: "Semester 2/2560", ca: 21.00, ce: 21.00, gpa: 2.79, totalGpa: 2.79 }
            ]
        },
        {
            semester: "1",
            year: "2561",
            enrolled: [
                { courseNo: "001201", courseTitle: "CRIT READ AND EFFEC WRITE", credit: 3.00, grade: "C" },
                { courseNo: "206281", courseTitle: "DISCRETE MATHEMATICS", credit: 3.00, grade: "D" },
                { courseNo: "953212", courseTitle: "DB SYS & DB SYS DESIGN", credit: 3.00, grade: "B" },
                { courseNo: "953233", courseTitle: "PROGRAMMING METHODOLOGY", credit: 3.00, grade: "C+" },
                { courseNo: "953261", courseTitle: "INTERACTIVE WEB DEVELOPMENT", credit: 2.00, grade: "B" },
                { courseNo: "953361", courseTitle: "COMP NETWORK & PROTOCOLS", credit: 3.00, grade: "F" }
            ],
            gpa: [
                { record: "Semester 1/2561", ca: 17.00, ce: 14.00, gpa: 1.85, totalGpa: 2.49 }
            ]
        },
        {
            semester: "2",
            year: "2561",
            enrolled: [
                { courseNo: "001225", courseTitle: "ENGL IN SCIENCE & TECH CONT", credit: 3.00, grade: "C+" },
                { courseNo: "206255", courseTitle: "MATH FOR SOFTWARE TECH", credit: 3.00, grade: "W" },
                { courseNo: "953201", courseTitle: "ALGO DESIGN & ANALYSIS", credit: 3.00, grade: "B" },
                { courseNo: "953214", courseTitle: "OS & PROG LANG PRINCIPLES", credit: 3.00, grade: "B" },
                { courseNo: "953232", courseTitle: "OO ANALYSIS & DESIGN", credit: 3.00, grade: "C+" },
                { courseNo: "953234", courseTitle: "ADVANCED SOFTWARE DEVELOPMENT", credit: 3.00, grade: "B" },
                { courseNo: "955200", courseTitle: "LEARNING THROUGH ACTIVITIES 2", credit: 1.00, grade: "B+" }
            ],
            gpa: [
                { record: "Semester 2/2561", ca: 19.00, ce: 16.00, gpa: 2.84, totalGpa: 2.57 }
            ]
        },
        {
            semester: "1",
            year: "2562",
            enrolled: [
                { courseNo: "011151", courseTitle: "REASONING", credit: 3.00, grade: "A" },
                { courseNo: "208263", courseTitle: "ELEMENTARY STATISTICS", credit: 3.00, grade: "D" },
                { courseNo: "259109", courseTitle: "TELECOM IN THAILAND", credit: 3.00, grade: "C" },
                { courseNo: "269111", courseTitle: "COMMU TECH IN A CHANGING WORLD", credit: 3.00, grade: "C+" },
                { courseNo: "953321", courseTitle: "SOFTWARE REQ ANALYSIS", credit: 3.00, grade: "C+" },
                { courseNo: "953322", courseTitle: "SOFTWARE DESIGN & ARCH", credit: 3.00, grade: "D+" },
                { courseNo: "953331", courseTitle: "COMPO-BASED SOFTWARE DEV", credit: 3.00, grade: "A" }
            ],
            gpa: [
                {
                    record: "Semester 1/2562", ca: 21.00, ce: 21.00, gpa: 2.50, totalGpa: 2.56
                }
            ]
        }
    ]

    const renderTable = (data, index1) => {
        return (
            <div key={index1} style={{ marginLeft: '20px' }}>
                <Title level={3} style={{ color: "#48DA9B" }}>{"Semester " + data.semester + "/" + data.year}</Title>
                <table className="table table-hover">
                    <thead className="thead-dark">
                        <tr>
                            <th>No.</th>
                            <th>Course No.</th>
                            <th>Course Title</th>
                            <th>Credit</th>
                            <th>Grade</th>
                        </tr>
                    </thead>
                    <tbody>
                        {data.enrolled.map((data, index) => {
                            return (
                                <tr key={index}>
                                    <td>{index + 1}</td>
                                    <td>{data.courseNo}</td>
                                    <td id="table-left">{data.courseTitle}</td>
                                    <td>{data.credit}</td>
                                    <td>{data.grade}</td>
                                </tr>
                            )
                        })
                        })}
                </tbody>
                </table>

                <br />
                <table className="table table-hover">
                    <thead className="thead-dark">
                        <tr>
                            <th>Record</th>
                            <th>CA</th>
                            <th>CE</th>
                            <th>GPA</th>
                            <th>Total GPA</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            data.gpa.map((data, index) => {
                                return (
                                    <tr key={index}>
                                        <td>{data.record}</td>
                                        <td>{data.ca}</td>
                                        <td>{data.ce}</td>
                                        <td>{data.gpa}</td>
                                        <td>{data.totalGpa}</td>
                                    </tr>
                                )
                            })
                        })}
                </tbody>
                </table>
            </div>
        )
    }

    return (
        <div>
            {TableData.map((data, index1) => {
                if (props.semester === "0" && props.year === "0") {
                    return renderTable(data, index1);
                }

                if (props.semester === "0" && props.year === data.year) {
                    return renderTable(data, index1);
                }

                if (props.year === "0" && props.semester === data.semester) {
                    return renderTable(data, index1);
                }

                if (props.semester === data.semester && props.year === data.year) {
                    return renderTable(data, index1);
                }
            })}
        </div>
    )
}

export default ReportTable;