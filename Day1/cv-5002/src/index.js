import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { BrowserRouter, Route, Redirect } from 'react-router-dom'
import Profile from './pages/Profile';
import Anime from './pages/Anime'
import HeaderPart from './components/HeaderPart';
import ProfileSide from './components/ProfileSide';
import StudyReport from './pages/StudyReport';
import 'antd/dist/antd.css';

const MainRouting =
    <BrowserRouter>
    <Route exact path="/" render={() => (
      <Redirect to="/profile"/>
    )}/>

        <Route path="/" component={HeaderPart} />
        <Route path="/profile" component={Profile}  />
        <Route path="/anime" component={Anime} />
        <Route path="/report" component={StudyReport} />
    </BrowserRouter>

ReactDOM.render(MainRouting, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister(); 
