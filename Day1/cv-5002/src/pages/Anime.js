import React, { useState, useEffect } from 'react';
import { Form, Button, Input, Spin, Empty, Row, Col, Typography, Pagination, Select, Layout, BackTop, Card, Tag } from 'antd';
import AnimeType from '../components/AnimeType';
import ProfileSide from '../components/ProfileSide';

const Anime = () => {
    const [animes, setAnimes] = useState([]);
    const [outputAnimes, setOutputAnimes] = useState([]);
    const [isLoading, setIsLoading] = useState();
    const [totalAnimes, setTotalAnimes] = useState();
    const [currentPage, setCurrentPage] = useState(1);
    const [pageSize, setPageSize] = useState(10);
    const [animeType, setAnimeType] = useState(AnimeType.ALL);
    const antIcon = <Spin size="large" />
    const { Title, Text } = Typography;
    const { Search } = Input;
    const { Option } = Select;
    const { Content } = Layout;

    useEffect(() => {
        (async () => {
            setIsLoading(true);
            try {
                let response = await fetch('https://api.jikan.moe/v3/search/anime?q=fate%20stay%20night%20unlimited&limit=1');
                let data = await response.json();
                if (data["results"].length === 0) {
                    setAnimes([]);
                    setOutputAnimes([]);
                    setIsLoading(false);
                } else {
                    setAnimes(data["results"]);
                    setOutputAnimes(data["results"]);
                    setIsLoading(false);
                }
                setTotalAnimes(data["results"].length);
            } catch (err) {
                console.log(err);
                setAnimes(-1);
                setOutputAnimes(-1)
                setIsLoading(false);
                setTotalAnimes(-1);
            }
        })();
    }, [])

    const fetchAnime = (animeTitle) => {
        setAnimes([]);
        setOutputAnimes([]);
        setIsLoading(true);
        (async () => {
            try {
                let response = await fetch('https://api.jikan.moe/v3/search/anime?q=' + animeTitle);
                let data = await response.json();
                if (data["results"].length === 0) {
                    setAnimes([]);
                    setOutputAnimes([]);
                    setIsLoading(false);
                } else {
                    setAnimes(data["results"]);
                    setOutputAnimes(data["results"]);
                    setIsLoading(false);
                }
                setTotalAnimes(data["results"].length);
            } catch (err) {
                console.log(err);
                setAnimes(-1);
                setOutputAnimes(-1)
                setIsLoading(false);
                setTotalAnimes(-1);
            }
        })();
    }

    const isInRange = (max, min, num) => {
        if (num <= max && num >= min) {
            return true;
        }
        return false;
    }

    const renderAnimeData = () => {
        if (outputAnimes.length === 0 && !isLoading) {
            return (
                <Empty image={Empty.PRESENTED_IMAGE_SIMPLE}
                    imageStyle={{
                        width: "30%",
                        height: "30%",
                        marginLeft: "35%"
                    }} description={
                        <span>
                            <Title level={2} style={{ color: "orange" }}>Not found the results.</Title>
                        </span>
                    } />
            );
        }

        let maxIndex = currentPage * pageSize - 1;
        let minIndex = currentPage * pageSize - pageSize;

        return outputAnimes.map((anime, index) => {
            if (!isInRange(maxIndex, minIndex, index)) {
                return null;
            }

            return (
                <Card hoverable id="background" style={{ marginBottom: "5%", boxShadow: "rgba(0,0,0,0.3) 0px 19px 38px, rgba(0,0,0,0.22) 0px 15px 12px" }}>
                    <Col>
                        <Row>
                            <Col className="card mb-3" id="background">
                                <Col className="row no-gutters box-shadow">
                                    <Col className="col-md-4">
                                        <img className="card-img" src={anime["image_url"]} />
                                    </Col>
                                    <Col className="col-md-8">
                                        <Col className="card-body">
                                            <Title level={2} className="card-title" style={{ color: "#48DA9B" }}>{anime["title"]}</Title>
                                            <Col>
                                                <Title level={3} style={{ color: "white" }}>Synopsis</Title>
                                            </Col>
                                            <p className="card-text">
                                                {anime["synopsis"]}
                                            </p>
                                            <Col>
                                                <p>Type: <Tag color='#454545' style={{ color: 'orange' }}>{anime["type"]}</Tag></p>
                                            </Col>
                                            <Col className="d-flex justify-content-end">
                                                <Button ghost><a href={anime["url"]}>Read more</a></Button>
                                            </Col>
                                        </Col>
                                    </Col>
                                </Col>
                            </Col>
                        </Row>
                    </Col>
                    <BackTop />
                </Card>
            );
        });
    }

    const changePage = (page, pageSize) => {
        setCurrentPage(page);
        window.scrollTo(0, 0);
    }

    const changePageSize = (current, size) => {
        setCurrentPage(current);
        setPageSize(size);
        window.scrollTo(0, 0);
    }

    const filterAnimeType = (animeType) => {
        setAnimeType(animeType);

        let size = 0;
        if (animeType !== AnimeType.ALL) {
            for (let i = 0; i < outputAnimes.length; i++) {
                if (animeType === animes[i].type)
                    size++;
            }
        } else {
            size = outputAnimes.length;
        }
        setTotalAnimes(size);

        let tempAnimes = [];
        if (animeType === AnimeType.ALL) {
            setOutputAnimes([...animes]);
        } else {
            for (let i = 0; i < outputAnimes.length; i++) {
                if (animes[i].type === animeType) {
                    tempAnimes.push(animes[i]);
                }
            }
            setOutputAnimes(tempAnimes);
        }
    }

    return (
        <Layout style={{ padding: '0px 100px' }} id="background">
            <ProfileSide />
            <Content>
                <Row type="flex" justify="center">
                    <Col span={24}>
                        <Row>
                            <Col span={20} offset={2}>
                                <Row>
                                    <Col offset={15} style={{ marginTop: '30px' }}>
                                        <Form layout="inline"
                                            onSubmit={(event) => event.preventDefault()} style={{ marginBottom: "5px" }}>
                                            <Form.Item style={{ marginRight: '5px' }}>
                                                <Search
                                                    placeholder="Anime title"
                                                    onSearch={value => fetchAnime(value)}
                                                />
                                            </Form.Item>
                                            <Form.Item>
                                                <Select defaultValue="All" style={{ width: 120 }} onChange={(value) => filterAnimeType(value)}>
                                                    <Option value={AnimeType.ALL}>{AnimeType.ALL}</Option>
                                                    <Option value={AnimeType.TV}>{AnimeType.TV}</Option>
                                                    <Option value={AnimeType.OVA}>{AnimeType.OVA}</Option>
                                                    <Option value={AnimeType.MOVIE}>{AnimeType.MOVIE}</Option>
                                                    <Option value={AnimeType.SPECIAL}>{AnimeType.SPECIAL}</Option>
                                                    <Option value={AnimeType.ONA}>{AnimeType.ONA}</Option>
                                                    <Option value={AnimeType.MUSIC}>{AnimeType.MUSIC}</Option>
                                                </Select>
                                            </Form.Item>
                                        </Form>
                                    </Col>
                                    <Col span={24} hidden={!isLoading} offset={12}>
                                        <Spin indicator={antIcon} />
                                    </Col>
                                    <Col span={24} id="placeholder">
                                        {renderAnimeData()}
                                        <Col className="d-flex justify-content-center">
                                            <Pagination
                                                style={{marginBottom:'20px'}}
                                                hideOnSinglePage
                                                defaultCurrent={1}
                                                total={totalAnimes}
                                                showSizeChanger onChange={(page, pageSize) => changePage(page, pageSize)}
                                                onShowSizeChange={(current, size) => changePageSize(current, size)} />
                                        </Col>
                                    </Col>
                                </Row>
                            </Col>
                        </Row>
                    </Col>
                </Row>
            </Content>
        </Layout>
    );
}

export default Anime;
