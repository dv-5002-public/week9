import React from 'react';
import { Divider, Typography, Icon, Layout, Carousel, Col } from 'antd';
import ProfileSide from '../components/ProfileSide';

const { Title, Text } = Typography;
const { Content } = Layout;

class Profile extends React.Component {
    render() {
        return (
            <Layout style={{ padding: '0px 100px' }} id="background">
                <ProfileSide />
                <Content id="background" style={{ paddingLeft: '10px' }}>
                    <div className="profileLine">
                        <div className="profileText">
                            <Text style={{ fontSize: '25px' }}><b>Name:</b> Kullanan Thanachotanan</Text>
                            <br />
                            <Text style={{ fontSize: '25px' }}><b>Nickname:</b> Nan</Text>
                            <br />
                            <Text style={{ fontSize: '25px' }}><b>Birthday:</b> 13 January 1999</Text>
                            <br />
                            <Text style={{ fontSize: '25px' }}><b>Age:</b> 21</Text>
                            <br />
                            <Text style={{ fontSize: '25px' }}><b>Faculty:</b> College of Arts, Media and Technology</Text>
                            <br />
                            <Text style={{ fontSize: '25px' }}><b>Major:</b> Software Engineer</Text>
                        </div>
                    </div>

                    <Divider style={{ marginLeft: "2%" }}>
                        <Icon type="slack-circle" theme="filled" style={{ color: "white", fontSize: '30px' }} />
                    </Divider>

                    <div className="block" id="normalFont">
                        <Title style={{ color: "#48DA9B" }}>Preference</Title>
                        <div className="philosophy_content">
                            <div id="space">
                                <img src="https://icons-for-free.com/iconfiles/png/512/game+controller+video+game+icon-1320087273514764593.png"
                                    id="icon" />
                                &nbsp;
                                <Text style={{ fontSize: '18px', color: 'white' }}>Play Game</Text>
                                &nbsp;&nbsp;
                        <img src="https://cdn2.iconfinder.com/data/icons/mixed-rounded-flat-icon/512/note-512.png"
                                    id="icon" />
                                &nbsp;
                                <Text style={{ fontSize: '18px', color: 'white' }}>Read Novel/Read Comics</Text>
                                &nbsp;&nbsp;
                        <img src="https://cdn1.iconfinder.com/data/icons/circle-outlines-colored/512/Music_Listen_Media_Song_Note-512.png"
                                    id="icon" />
                                &nbsp;
                                <Text style={{ fontSize: '18px', color: 'white' }}>Listen Japanese Song</Text>
                            </div>
                        </div>
                    </div>

                    <Divider style={{ marginLeft: "2%" }}>
                        <Icon type="slack-circle" theme="filled" style={{ color: "white", fontSize: '30px' }} />
                    </Divider>

                    <div className="block" id="normalFont">
                        <Title style={{ color: "#48DA9B" }}>Speciality</Title>
                        <u style={{ fontSize: '18px' }}>Language</u>
                        <u style={{ marginLeft: "25%", fontSize: '18px' }}>Programming Language</u>
                        <u style={{ marginLeft: "25%", fontSize: '18px' }}>Others</u>
                        <div id="space">
                            <img src="https://image.flaticon.com/icons/svg/1377/1377975.svg"
                                id="icon" />
                            &nbsp;
                        <Text style={{ fontSize: '18px', color: 'white' }}>English</Text>
                            &nbsp;
                    <img src="https://cdn0.iconfinder.com/data/icons/world-flags-6-2/100/Japan-512.png" id="icon" />
                            &nbsp;
                            <Text style={{ fontSize: '18px', color: 'white' }}>Japanese</Text>

                            <img src="../5002Assets/java-icon.png" id="icon" style={{ marginLeft: "15%" }} />
                            &nbsp;
                            <Text style={{ fontSize: '18px', color: 'white' }}>Java</Text>
                            &nbsp;
                    <img src="../5002Assets/js-icon.png" id="icon" />
                            &nbsp;
                            <Text style={{ fontSize: '18px', color: 'white' }}>JavaScript</Text>
                            &nbsp;
                    <img src="https://cdn2.iconfinder.com/data/icons/designer-skills/128/react-512.png" id="icon" />
                            &nbsp;
                            <Text style={{ fontSize: '18px', color: 'white' }}>React</Text>

                            <img src="https://cdn2.iconfinder.com/data/icons/social-icon-3/512/social_style_3_html5-512.png" id="icon" style={{ marginLeft: "17%" }} />
                            &nbsp;
                            <Text style={{ fontSize: '18px', color: 'white' }}>HTML</Text>
                            &nbsp;
                            <img src="https://www.jamesstone.com/wp-content/uploads/2017/11/Boostrap_logo.svg" id="icon" />
                            &nbsp;
                            <Text style={{ fontSize: '18px', color: 'white' }}>Bootstrap</Text>
                            &nbsp;
                        </div>
                    </div>

                    <Divider style={{ marginLeft: "2%" }}>
                        <Icon type="slack-circle" theme="filled" style={{ color: "white", fontSize: '30px' }} />
                    </Divider>

                    <Title style={{ color: "#48DA9B" }}>History</Title>
                    <Col offset={2}>
                        <Carousel autoplay>
                            <div>
                                <h3><img src="../5002Assets/3.jpg" className="d-block w-100" /></h3>
                            </div>
                            <div>
                                <h3><img src="../5002Assets/4.jpg" className="d-block w-100" /></h3>
                            </div>
                            <div>
                                <h3><img src="../5002Assets/5.jpg" className="d-block w-100" /></h3>
                            </div>
                        </Carousel>
                    </Col>
                </Content>
            </Layout>
        )
    }
}

export default Profile