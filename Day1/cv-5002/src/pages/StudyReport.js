import React from 'react';
import ReportTable from '../components/ReportTable';
import { Button, Layout, Col, Row, Typography } from 'antd';
import ProfileSide from '../components/ProfileSide';

const { Title } = Typography;
const { Content } = Layout;

class StudyReport extends React.Component {
    state = ({
        semester: "0",
        year: "0"
    })

    handleSemester = ({ target }) => {
        console.log(target.value)
        this.setState({
            semester: target.value
        });
    }

    handleYear = ({ target }) => {
        this.setState({
            year: target.value
        });

    }

    resetSearch = () => {
        this.setState({
            semester: "0",
            year: "0"
        });

    }

    render() {
        return (
            <Layout style={{ padding: '0px 100px' }} id="background">
                <ProfileSide />
                <Content>
                    <Title level={2} style={{ color: "#48DA9B", marginLeft: '20px' }}>Study Report</Title>
                    <Row style={{ marginLeft: '20px' }}>
                        <Col span={5}>
                            <div className="input-group mb-3">
                                <select className="custom-select" id="selectorSemester" defaultValue="0" onChange={this.handleSemester}>
                                    <option value="0">All semester</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                </select>
                            </div>
                        </Col>
                        <Col span={5} offset={1}>
                            <div className="input-group mb-3">
                                <select className="custom-select" id="selectorYear" defaultValue="0" onChange={this.handleYear}>
                                    <option value="0">All year</option>
                                    <option value="2560">2560</option>
                                    <option value="2561">2561</option>
                                    <option value="2562">2562</option>
                                </select>
                            </div>
                        </Col>
                        <Col span={2} offset={1}>
                            <Button type="danger" onClick={this.resetSearch}>Reset</Button>
                        </Col>
                    </Row>
                    <ReportTable semester={this.state.semester} year={this.state.year} />
                </Content>
            </Layout>
        )
    }
}

export default StudyReport