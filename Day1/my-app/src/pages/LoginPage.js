import React from 'react';

class LoginPage extends React.Component {
    render() {
        return (
            <div>
                <h2>Login Page</h2>
                <div>
                    <div>
                        Username <input type="text" placeholder="username" />
                    </div>
                    <div>
                        Password <input type="text" placeholder="password" />
                    </div>
                    <div>
                        <input type="button" value="Login" onClick={
                            () => {
                                window.location = "/processLogin"
                            }
                        } />
                    </div>
                </div>
            </div>
        )
    }
}

export default LoginPage