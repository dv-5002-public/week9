import React from 'react';
import { BrowserRouter, Route, Redirect } from 'react-router-dom'
import UserPage from './UserPage';
import HomePage from './HomePage';
import Header from '../components/Header';
import LoginPage from './LoginPage';
import GpaPage from './GpaPage';
import PrivateRoute from '../components/PrivateRoute'
import UserPageFunc from './UserPageFunc';

const MainRouting = () => {
    return (
        <BrowserRouter>
            <Route path="/processLogin" render={() => {
                localStorage.setItem("isLogin", true)
                return <Redirect to="/gpa" />
            }} />
            <Route path="/processLogout" render={() => {
                localStorage.setItem("isLogin", false)
                return <Redirect to="/home" />
            }} />
            <Route path="/" component={Header} />
            <Route path="/login" component={LoginPage} />
            <Route path="/home" component={HomePage} />
            <Route path="/users" component={UserPage} />

            <PrivateRoute path="/gpa" component={GpaPage} />
        </BrowserRouter>
    )
}

export default MainRouting