import React, { useState, useEffect } from 'react';
import { Select } from 'antd';

const UserPage = () => {

    const { Option } = Select;

    const handleChange = (value) => {
        console.log(`selected ${value}`);
    }

    const [users, setUsers] = useState([])
    const [selectedIndex, setSelectedIndex] = useState(-1)

    const fetchUser = () => {
        fetch('https://jsonplaceholder.typicode.com/users')
            .then(response => response.json())
            .then(data => {
                setUsers(data)
            })
            .catch(error => console.log(error))
    }

    const onSelectedDropDown = (event) => {
        alert(event.target.value)
        const index = event.target.value
        setSelectedIndex(index)
    }

    useEffect(() => {
        fetchUser()
    }, [])

    return (
        <div>
            <div><h2>User Page</h2></div>
            <div>
                <Select defaultValue="lucy" style={{ width: 120 }} onChange={handleChange}>
                    <Option value="jack">Jack</Option>
                    <Option value="lucy">Lucy</Option>
                    <Option value="disabled" disabled>
                        Disabled
                    </Option>
                    <Option value="Yiminghe">yiminghe</Option>
                </Select>

            </div>,
            {/* <select onChange={(event) => alert(event.target.value) >
                <option value={-1}>All Users</option>
                {
                    users.map((item, index) => {
                    return (
                        <div key={index}>

                        </div>
                    )
                })
                }
            </select> */}
            <div>
                {
                    // selectedIndex == -1 ?
                    users.map((item, index) => {
                        return (
                            <div key={index}>
                                <p>Id: {item.id}</p>
                                <p>Name: {item.name}</p>
                                <p>Username: {item.username}</p>
                                <p>Email: {item.email}</p>
                                <hr />
                            </div>
                        )
                    })
                    // :
                    // <div key={index}>
                    //     <p>Id: {users[selectedIndex]}</p>
                    //     <p>Name: {item.name}</p>
                    //     <p>Username: {item.username}</p>
                    //     <p>Email: {item.email}</p>
                    //     <hr />
                    // </div>
                }
            </div>
        </div>
    )
}

export default UserPage