import React, { useState, useEffect } from 'react';

const UserPageFunc = () => {

    const [users, setUsers] = useState([])

    useEffect(()=>{
        fetchUser()
        console.log('useEffect')
    },[])

    const fetchUser = () => {
        fetch('https://jsonplaceholder.typicode.com/users')
            .then(response => response.json())
            .then(data => {
                setUsers(data)
                // this.setState({ users: data })
            })
            .catch(error => console.log(error))
    }

    return (
        <div>
            <div><h2>User Page</h2></div>
            <div>
                {
                    users.map((item, index) => {
                        return (
                            <div key={index}>
                                <p>Id: {item.id}</p>
                                <p>Name: {item.name}</p>
                                <p>Username: {item.username}</p>
                                <p>Email: {item.email}</p>
                                <hr />
                            </div>
                        )
                    })
                }
            </div>
        </div>
    )
}

export default UserPageFunc