import React, { useState, useEffect } from 'react';
import { Descriptions, List, Card, Layout } from 'antd';

const AlbumListPage = (props) => {
    const [user, setUser] = useState({})
    const [albumList, setAlbumList] = useState([])
    const { Content } = Layout;

    const fetchUserData = () => {
        const userId = props.match.params.user_id
        fetch('https://jsonplaceholder.typicode.com/users/' + userId)
            .then(response => response.json())
            .then(data => {
                setUser(data)
            })
            .catch(error => console.log(error))
    }

    const fetchAlbumListData = () => {
        const userId = props.match.params.id
        fetch('https://jsonplaceholder.typicode.com/photos?albumId=' + userId)
            .then(response => response.json())
            .then(data => {
                setAlbumList(data)
            })
            .catch(error => console.log(error))
    }

    //componentDidMount
    useEffect(() => {
        fetchUserData()
        fetchAlbumListData()
    }, [])


    return (
        <Content style={{ padding: '50px 50px' }}>
            <h1 style={{ textAlign: "center" }}>Gallery Page</h1>
            <Descriptions title={"User : " + user.name}>
                <Descriptions.Item label="Email">{user.email}</Descriptions.Item>
                <Descriptions.Item label="Telephone">{user.phone}</Descriptions.Item>
                <Descriptions.Item label="Website">{user.website}</Descriptions.Item>
            </Descriptions>

            <List
                grid={{ gutter: 15, column: 3 }}
                dataSource={albumList}
                renderItem={item => (
                    <List.Item>
                        <Card title={item.title}><img src={item.url} style={{ width: "100%", height: "200px" }} /></Card>
                    </List.Item>
                )}
            >
            </List>
        </Content>

    )
}

export default AlbumListPage