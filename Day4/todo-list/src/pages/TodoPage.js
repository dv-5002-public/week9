import React, { useState, useEffect } from 'react';
import { List, Typography, Button, Descriptions, Select, Layout } from 'antd';

const TodoPage = (props) => {
    const { Option } = Select;
    const [user, setUser] = useState([]);
    const [todoList, setTodoList] = useState([]);
    const [selected, setSelected] = useState(-1);
    const { Content } = Layout;

    useEffect(() => {
        console.log('useEffect')
        fetchUsersData();
        fetchTodoData();
    }, [])

    const fetchUsersData = () => {
        const userId = props.match.params.user_id;
        fetch('http://jsonplaceholder.typicode.com/users/' + userId)
            .then(response => response.json())
            .then(data => {
                setUser(data)
            })
            .catch(error => console.log(error));
    }

    const fetchTodoData = () => {
        const userId = props.match.params.user_id;
        fetch('http://jsonplaceholder.typicode.com/todos?userId=' + userId)
            .then(response => response.json())
            .then(data => {
                setTodoList(data)
            })
            .catch(error => console.log(error));
    }

    const doneStatus = (value) => {
        let newList = [...todoList];
        newList[value].completed = true;
        setTodoList(newList)
    }

    const onSelectChange = (value) => {
        // alert(value)
        setSelected(value)
        console.log(value)
    }

    console.log('User data ', user)
    console.log('Todo List', todoList)
    return (
        <Content style={{ padding: '50px 50px' }}>
            <Descriptions title="User Info" >
                <Descriptions.Item label="Name">{user.name}</Descriptions.Item>
                <Descriptions.Item label="User Name">{user.username}</Descriptions.Item>
                <Descriptions.Item label="Email">{user.email}</Descriptions.Item>
                <Descriptions.Item label="Telephone">{user.phone}</Descriptions.Item>
            </Descriptions>
            <Select defaultValue="All" style={{ width: 120 }} onChange={onSelectChange}>
                <Option value={-1}>All</Option>
                <Option value={true}>Done</Option>
                <Option value={false}>Doing</Option>
            </Select>

            <List
                header={<div>Todo List</div>}
                footer={<div>Footer</div>}
                bordered
                dataSource={
                    selected == -1 ?
                        todoList
                        :
                        todoList.filter(m => m.completed == selected)
                }
                renderItem={(item, index) => (
                    <List.Item>
                        <div>
                            {
                                item.completed ?
                                    <Typography.Text delete >DONE {item.title}</Typography.Text>
                                    :
                                    <Typography.Text mark > DOING {item.title}</Typography.Text>
                            }
                        </div>
                        <div>
                            {
                                item.completed ?
                                    <div></div>
                                    :
                                    <Button style={{ float: 'right' }}
                                        value={index} onClick={() => doneStatus(item.id - 1)} >
                                        <span>Done</span>
                                    </Button>
                            }
                        </div>
                    </List.Item>
                )}
            />
        </Content>
    )
}
export default TodoPage;